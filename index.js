// console.log("HI");

/*
	Objects
		- an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalites.

	Creating objects using object literal:
		Syntax:
			let objectName = {
				keyA: ValueA,
				keyB: ValueB
			};
*/

let student = {
	firstName: "Rupert",
	lastName: "Ramos",
	age: 30,
	studentID: "2022-009752",
	email: ["rupert.ramos@mail.com", "RBR1209@gmail.com"],
	address: {
		street: "125 Ilang-Ilang street",
		city: "Quezon City",
		country: "Philippines"
	}
};

console.log("Result from creating an object:");
console.log(student);
console.log(typeof student);

// Creating Objects using Constractor Function
/*
	- creates a reusable function to create several objects that have the same data structures. This is useful for creating multiple instances or copies of an object.

	Syntax:
		function objectName(valueA, valueB) {
			this.keyA = valueA
			this.keyB = valueB
		};

		let variable = new function objectName(valueA, valueB);
		console.log(variable);

	this
		- is a keyword that is used for invoking. It refers to global object.
		- don't forget to add "new" keyword when we are creating the varaibles.
*/
// We use Pascal Casing for the ObjectName when creating objects using constructor function.
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating object using object constructor:");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", [2020, 2021]);
console.log("Result of creating object using object constructor:");
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result of creating object using object constructor:");
console.log(oldLaptop); // undefine with "new"

// Creating empty object as placeholder
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer)

/*MINI ACTIVITY
	- create an object constructor function to produce 2 objects with 3 key-value pairs.
	- log the 2 new objects in the console and send SS in our GC.
*/

function ProduceObject(obj1, obj2, obj3){
	this.obj1 = obj1;
	this.obj2 = obj2;
	this.obj3 = obj3;
}

let produceObject = new ProduceObject("One", "Two", "Three")
let produceObject2 = new ProduceObject("Four", "Five", "Six")
console.log(produceObject)
console.log(produceObject2)

// Solution ni RR
function Camera(brand, pixels, manufactureDate) {
	this.brand = brand;
	this.pixels = pixels;
	this.manufactureDate = manufactureDate
};

let myCamera = new Camera("Nikon", "40MP", 2019);
let myFriendsCamera = new Camera("Canon", "20MP", 2018);
console.log("New Objects created using object constructor.")
console.log(myCamera);
console.log(myFriendsCamera);

// Accessing Object Property
// Using the dot notation
// Syntax: objectName.propertyName
console.log("Result from dot notation: " + myLaptop.name);

// Using the bracket notation
// Syntax: objectName["name"];
console.log("Result from bracket notation: "+ myLaptop["name"]);

// Accessing array objects

let array = [laptop, myLaptop];
// let array = [{name: "Lenovo", manufactureDate: 2008}, {name: "MacBook Air", manufactureDate: [2019, 2020]}];

// Dot Notation
console.log(array[0].name);

// square bracket notation
console.log(array[0]["name"]);

// Initializing/Adding/Deleting/Reassigning object properties
let car = {};
console.log(car);

// Adding object properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation:")
console.log(car);

car["manufacture date"] = 2019;
console.log(car);

car.name = ["Ferrari", "Toyota"]

// Deleting object properties
delete car["manufacture date"]; 
// car["manufacture date"] = " ";//can be use to delete
console.log("Result from deleting object properties:");
console.log(car);

// Reassigning object properties
car.name = "Tesla";
console.log("Result from reassigning object propery:");
console.log(car);

console.log("");
/*
	Object Method
		- a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are function related to a specific object property.
*/

let person = {
	name: "John",
	talk: function() {
		console.log("Hello! My name is " + this.name);
	}
};
console.log(person);
console.log("Result from object methods:");
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk();
console.log(person);

let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	eamils: ["johnD@gmail.com", "joe12@yahoo.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
	
};
friend.introduce();

// Real world application
/*
	Scenario:
		1. We would like to create a game that would have several pokemons to interact with each other.
		2. every pokemnon would have the same sets of stats, properties and functions.
*/
// Using object literals

console.log("")
console.log("Activity starts after this line.");

// Part 1
// 1. Initialize/add the following trainer object properties:
//       Name (String)
//       Age (Number)
//       Pokemon (Array)
//       Friends (Object with Array values for properties)
//     2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
//     3. Access the trainer object properties using dot and square bracket notation.
//     4. Invoke/call the trainer talk object method.
let trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	hoenn: ["May", "Max"],
	kanto: ["Brock", "Misty"]
}
trainer.talk = function() {
	console.log("Pickachu! I choose you!");
}
console.log(trainer);
console.log(trainer.name);
console.log(trainer["name"]);
console.log(trainer.age);
console.log(trainer["age"]);
console.log(trainer.pokemon);
console.log(trainer["pokemon"]);
console.log(trainer.friends);
console.log(trainer["friends"]);

trainer.talk();

// Part 2
// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
// 	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
// 	(target.health - this.attack)

// 2.) If health is less than or equal to 5, invoke faint function
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackles targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth.")
	},
	faint: function() {
		console.log("Pikachu fainted")
	}
};
console.log(myPokemon);
// Using object constructor
function Pokemon(name, level) {
	// Properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack =  level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		newHealth = target.health - this.attack;
		if (newHealth <= 5) {
			target.faint();
			return target.health = newHealth;
		} else {
			return target.health = newHealth;
		}
	},
	this.faint = function() {
		console.log(this.name + " fainted.");
	}
};

let charizard = new Pokemon ("Charizard", 12);
let squirtle = new Pokemon ("Squirtle", 6);

console.log(charizard);
console.log(squirtle);

// interact
charizard.tackle(squirtle);
charizard.tackle(squirtle);
console.log("")
squirtle.tackle(charizard);
squirtle.tackle(charizard);
squirtle.tackle(charizard);
squirtle.tackle(charizard);
squirtle.tackle(charizard);
squirtle.tackle(charizard);

